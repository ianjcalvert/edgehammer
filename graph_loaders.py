from csv import DictReader
from math import ceil
import networkx as nx
import numpy as np


def load_from_csv(node_file, edge_file):
    nodes = {}
    for node in DictReader(open(node_file)):
        node["x"] = float(node["x"])
        node["id"] = int(node["id"])
        node["y"] = float(node["y"])
        nodes[node["id"]] = node

    min_x = min(float(node["x"]) for node in nodes.values())
    max_x = max(float(node["x"]) for node in nodes.values())
    min_y = min(float(node["y"]) for node in nodes.values())
    max_y = max(float(node["y"]) for node in nodes.values())


    for node in nodes.values():
        node["x"] = ((node["x"] - min_x) / (max_x - min_x))
        node["y"] = ((node["y"] - min_y) / (max_y - min_y))

    edges = []
    for edge in DictReader(open(edge_file)):
        edges.append((int(edge["source"]), int(edge["target"]), float(edge.get("weight", 1))))

    edge_segments = []
    for edge in edges:
        source = nodes[edge[0]]
        target = nodes[edge[1]]
        weight = edge[2]
        segments = [[source["x"], source["y"], weight], [target["x"], target["y"], weight]]
        edge_segments.append(np.array(segments))
    return edge_segments

def load_from_graphml(filename):
    """
    Warning: this returns more information than the csv loader and is intended
    as a guide. It has been used for loading and then rendering with the cairo
    renderer, but is slightly different from the csv loader
    """
    network = nx.read_graphml(open(filename))
    nodes = {}
    node_info_list = []
    for nxnode in network.nodes():
        node = network.node[nxnode]
        node["x"] = float(node["x"])
        node["id"] = nxnode
        node["size"] = float(node["weight"])
        node["colour"] = change_rgb(float(node["r"]),float(node["g"]),float(node["b"]))
        node["y"] = float(node["y"])
        nodes[node["id"]] = node


    min_x = min(float(node["x"]) for node in nodes.values()) - 250
    max_x = max(float(node["x"]) for node in nodes.values()) + 250
    min_y = min(float(node["y"]) for node in nodes.values()) - 250
    max_y = max(float(node["y"]) for node in nodes.values()) + 250
    print(min_x, min_y, max_x, max_y)


    for node in nodes.values():
        node["x"] = ((node["x"] - min_x) / (max_x - min_x))
        node["y"] = ((node["y"] - min_y) / (max_y - min_y))
        node_info_list.append([node["x"], node["y"],
                              node["colour"], node["size"]])

    edges = []
    colours = []
    for edge in network.edges():
        edges.append(edge)
        source = nodes[edge[0]]["colour"]
        target = nodes[edge[1]]["colour"]
        colours.append((source, target))

    edge_segments = []
    edge_weights = []
    for edge in edges:
        edge_weights.append(float(network.get_edge_data(*edge)["weight"]))
        source = nodes[edge[0]]
        target = nodes[edge[1]]
        segments = [[source["x"], source["y"]], [target["x"], target["y"]]]
        edge_segments.append(np.array(segments))
    return edge_segments, colours, node_info_list, edge_weights

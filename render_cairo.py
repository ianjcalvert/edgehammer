"""
This file is an initial dump of code for rendering with cairo

There's a lot of flexibility with this, but it's slower and is useful mostly
for smaller renders.

TODO: this must be cleaned up for delivery.
"""


def save_image(name, edge_segments, colours, nodes, edge_weights, width, height, lw=0.1, alpha=0.1,bounding_box = None):
    if bounding_box is None:
        bounding_box = (0, 0, width, height)
    x0, y0, x1, y1 = bounding_box
    bb_width = x1 - x0
    bb_height = y1 - y0
    surface = cairo.ImageSurface (cairo.FORMAT_ARGB32, x1 - x0, y1 - y0)
    ctx = cairo.Context (surface)
    ctx.set_source_rgb(1,1,1) # transparent black
    ctx.rectangle(0, 0, x1 - x0, y1 - y0)
    ctx.fill()
    #ctx.scale (WIDTH, HEIGHT) # Normalizing the canvas

    ctx.set_source_rgb (0,0,0) # Solid color
    ctx.set_line_width (lw)
    ctx.set_line_cap(cairo.LINE_CAP_ROUND)
    ctx.stroke ()
    max_weight = max(edge_weights)
    min_weight = min(edge_weights)
    total_segments=  len(edge_segments)
    for i, segments in tqdm(enumerate(edge_segments), total=total_segments):
        if segments[0][0] == segments[-1][0] and segments[0][1] == segments[-1][1]:
            continue
        weight = (edge_weights[i] - min_weight) / max_weight
        weight *= 50
        weight += 0.1
        ctx.set_line_width (weight)
        start_colour, end_colour = colours[i]
        linear = cairo.LinearGradient(segments[0][0] * width - x0, segments[0][1] * height - y0,
                                      segments[-1][0] * width - x0, segments[-1][1] * height - y0)
        linear.add_color_stop_rgba(0,start_colour[0],start_colour[1],start_colour[2],alpha)
        linear.add_color_stop_rgba(1,end_colour[0],end_colour[1],end_colour[2],alpha)
        ctx.set_source(linear)
        #ctx.set_source_rgba(start_colour[0],start_colour[1],start_colour[2],alpha)
        drawing = False
        last_point = (segments[0][0] * width - x0, segments[0][1] * height - y0)
        last_x = segments[0][0] * width - x0
        last_y = segments[0][1] * width - y0
        last_in_bb = (0 <= last_x <= bb_width and 0 <= last_y <= bb_height)
        total_segments = len(segments)
        for j, point in enumerate(segments[1:]):
            dist_to_end = 0.5 - min(j / len(segments), (len(segments) - j) / len(segments))
            dist_to_end += 0.5
            ctx.set_line_width(weight * dist_to_end)
            point_x = point[0]*height - x0
            point_y = point[1]*width - y0
            in_bb = 0 <= point_x <= bb_width and 0 <= point_y <= bb_height
            if in_bb or last_in_bb:
                if not drawing:
                    ctx.move_to(last_x, last_y)
                ctx.move_to(last_x, last_y)
                ctx.line_to(point_x, point_y)
                drawing = True
            else:
                drawing = False
            last_edge = [(last_x, last_y), (point_x, point_y)]
            last_x, last_y = point_x, point_y
            last_in_bb = in_bb
            ctx.stroke()
        if last_in_bb:
            ctx.line_to(last_x, last_y)
        ctx.stroke()
    ctx.set_line_width(2)
    if False:
        for i, node in tqdm(enumerate(nodes), total=len(nodes)):
            ctx.set_source_rgba(node[2][0], node[2][1], node[2][2], 1)
            size = node[3] ** 0.5
            size /= 161
            size *= 150
            size += 10

            ctx.arc(node[0]*width - x0, node[1]*height - y0, size, 0, pi*2)
            ctx.fill()
            ctx.set_source_rgba(0,0,0,1)
            ctx.arc(node[0]*width - x0, node[1]*height - y0, size, 0, pi*2)
            ctx.stroke()

    surface.write_to_png (name) # Output to PNG
    return

import datashader as ds
import datashader.transfer_functions as tf
from datashader.colors import *
from matplotlib.colors import rgb2hex
from matplotlib.cm import get_cmap
import xarray as xr
from tqdm import tqdm

import numpy as np
from pandas import DataFrame, Index

from datashader.utils import export_image 

def save_image(name, edge_segments, width=4000, height=4000, count=False):
    # Need to put a [np.nan, np.nan, np.nan] between edges
    def edge_iterator():
        for edge in edge_segments:
            yield edge
            yield np.array([[np.nan, np.nan, np.nan]])
    merged = np.concatenate(list(edge_iterator()))
    arr = DataFrame(merged)
    arr.columns = ['x', 'y', 'weight']
    opts={}
    cvs = ds.Canvas(plot_width=width, plot_height=height, x_range=(0, 1), y_range=(0, 1))
    if count:
        agg = cvs.line(arr, 'x', 'y',  ds.count())
    else:
        agg = cvs.line(arr, 'x', 'y',  ds.sum('weight'))
    img = tf.shade(agg, cmap=inferno, **opts)
    img2 = tf.set_background(img, 'black')
    export_image(img2, name)

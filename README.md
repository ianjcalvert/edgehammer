# Overview

The main goal of this project is to take a large and unweidly graph and group edges together to make connections between larger groups clearer.

It's also fairly pretty.

# Approach

The key approach is to take each edge and split it into a series of smaller edges.
Each point on the edge attracts all other points on other edges.

A simple way of doing this scales very poorly, as each point needs to be checked against every other one.

Instead, we can calculate the global impact of each point.

By taking each point, and drawing a single pixel for each one (accumulating where they overlap)
and then blurring the whole picture, we get essentially the same result.
Computing gradients over this then lets us have a single reference that shows how points should
move.

This scales linearly up as the number of edge points increases for drawing, and the blurring and gradient calculations are dependent only on the accuracy desired.

The densities can be calculated independently and summed too, allowing distribution over
multiple processes / machines.

# Installation


    conda env create -f environment.yml

## Run an example

There is an example setup in main.py

   python -m examples.gtr

There should now be a result saved as `bundled_edges_gtr.png`


## Running something on a larger cluster using dask

This depends on dask, start a scheduler

    dask-scheduler

And start a worker, using processes not threads

    dask-worker localhost:8786 --nthreads=1 --nprocs=12

Now you can load your graph

```python
from graph_loaders import *
edges = load_from_csv("node_list.csv", "edge_list.csv")
```

Connect to the cluster

```python
from dask.distributed import Client
client = Client("scheduler-host:8786")
```

Bundle the edges

```python
from core import bundle
bundled_edges = bundle(edges)
```

And finally render the result out

```python
from render_datashader import save_image
save_image("image_output_name", bundled_edges)
```

# Next steps

The code is in an early state, being kind.

* The section where the most time (almost half) is spent is resampling the edges. The calculations and process is simple, I believe the time is taken largely with just copying data over. It is also done in two steps as having an unknown length output structure wasn't something I could make work in two steps, meaning lots of things are re-calculated. I am certain this could be highly optimised.
* Distributing the rendering of the edges when the total number gets very high would likely improve performance, as instead of bringing back all the edges themselves you'd have a fixed size return.
* Not splitting the edges into batches randomly, but trying to ensure batches take roughly the same amount of time to process.
* If edges have stopped moving while looping through the advect and resample, don't keep reprocessing them.
* The algorithm currently treats any and all edges as equal.
    * Edge weights should be considered both in tension (for moving and smoothing) and for the strength of the attraction. For the attraction this can be done simply by increasing the amount added to the accumulator. Scaling is likely to be important here, and log scaling might help some datasets.
    * Edge size and/or direction should be considered. By splitting the accumulator into multiple 'layers', and drawing edges to them according to their angle (blending smoothly from one layer to the next, so a 45 degree edge is half drawn in one layer and half in another), edges would mostly only bundle with other *similar* edges.
* Node points themselves can have a negative value palced in the accumulator, which means the nodes "push" edges around them, this helps display if you have nodes rendered as circles. 



from dask.distributed import Client

from graph_loaders import *
from render_datashader import save_image
from core import bundle

if __name__ == '__main__':
    client = Client()
    edges = load_from_csv("examples/data/gtr/nodes.csv", "examples/data/gtr/edges.csv")

    bundled_edges = bundle(edges, 0.05, 0.7)
    save_image("bundled_edges_gtr", bundled_edges, width=2000, height=2000)

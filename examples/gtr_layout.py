from csv import DictReader
import networkx as nx
from math import log
from tqdm import tqdm
import numpy as np
from dask.distributed import Client

from scipy.sparse import spdiags, coo_matrix
import scipy as sp
from core import bundle
from render_datashader import save_image

def forceatlas2_layout(G, iterations=10, linlog=False, pos=None, nohubs=False,
                       kr=0.001, k=None, dim=2):
    """
    Options values are
    g                The graph to layout
    iterations       Number of iterations to do
    linlog           Whether to use linear or log repulsion

    Code is from https://github.com/tpoisot/nxfa2
    """
    # We add attributes to store the current and previous convergence speed
    for n in G:
        G.node[n]['prevcs'] = 0
        G.node[n]['currcs'] = 0
        # To numpy matrix
    # This comes from the spares FR layout in nx
    A = nx.to_scipy_sparse_matrix(G, dtype='f')
    nnodes, _ = A.shape

    try:
        A = A.tolil()
    except Exception as e:
        A = (coo_matrix(A)).tolil()
    if pos is None:
        pos = np.asarray(np.random.random((nnodes, dim)), dtype=A.dtype)
    else:
        pos = pos.astype(A.dtype)
    if k is None:
        k = np.sqrt(1.0 / nnodes)
        # Iterations
    # the initial "temperature" is about .1 of domain area (=1x1)
    # this is the largest step allowed in the dynamics.
    t = 0.1
    # simple cooling scheme.
    # linearly step down by dt on each iteration so last iteration is size dt.
    dt = t / float(iterations + 1)
    displacement = np.zeros((dim, nnodes))
    for iteration in tqdm(range(iterations)):
        displacement *= 0
        # loop over rows
        for i in range(A.shape[0]):
            # difference between this row's node position and all others
            delta = (pos[i] - pos).T
            # distance between points
            distance = np.sqrt((delta ** 2).sum(axis=0))
            # enforce minimum distance of 0.01
            distance = np.where(distance < 0.01, 0.01, distance)
            # the adjacency matrix row
            Ai = np.asarray(A.getrowview(i).toarray())
            # displacement "force"
            Dist = k * k / distance ** 2
            if nohubs:
                Dist = Dist / float(Ai.sum(axis=1) + 1)
            if linlog:
                Dist = np.log(Dist + 1)
            displacement[:, i] += \
                (delta * (Dist - Ai * distance / k)).sum(axis=1)
            # update positions
        length = np.sqrt((displacement ** 2).sum(axis=0))
        length = np.where(length < 0.01, 0.01, length)
        pos += (displacement * t / length).T
        # cool temperature
        t -= dt
        # Return the layout
    return dict(zip(G, pos))

if __name__ == "__main__":
    node_numbers = {}
    nodes = {}
    edges = []
    for node in DictReader(open("examples/data/gtr_unlaid/gtr_nodes.csv")):
        node_numbers[node["node_id"]] = len(node_numbers)

    network_edges = []
    for i, edge in tqdm(enumerate(DictReader(open("examples/data/gtr_unlaid/gtr_edges.csv")))):
        funding = float(edge["fund"])
        if funding > 0:
            edges.append((node_numbers[edge["source"]], node_numbers[edge["target"]], {'weight': funding}))
        if funding > 5e6:
            network_edges.append(edges[-1])

    network = nx.Graph()
    network.add_edges_from(network_edges)
    print("Selecting the largest subgraph")
    largest = sorted(nx.connected_component_subgraphs(network), key = len, reverse=True)[0]

    print("Edges and nodes loaded, laying out largest component", len(largest))
    layout = forceatlas2_layout(largest, linlog=True, iterations=50, nohubs=True)


    min_x = min(float(node[0]) for node in layout.values())
    max_x = max(float(node[0]) for node in layout.values())
    min_y = min(float(node[1]) for node in layout.values())
    max_y = max(float(node[1]) for node in layout.values())
    print(min_x, min_y, max_x, max_y)

    for i, node in layout.items():
        layout[i] = ((node[0] - min_x) / (max_x - min_x), (node[1] - min_y) / (max_y - min_y))

    edge_segments = []
    for edge in  largest.edges():
        source = layout[edge[0]]
        target = layout[edge[1]]
        weight = network.get_edge_data(edge[0], edge[1])['weight']
        segments = [[source[0], source[1], weight], [target[0], target[1], weight]]
        edge_segments.append(np.array(segments))

    save_image("edges_gtr_fa2", edge_segments, width=2000, height=2000)
    save_image("edges_gtr_fa2_count", edge_segments, width=2000, height=2000, count=True)
    client = Client()
    bundled_edges = bundle(edge_segments, 0.05, 0.7)
    save_image("bundled_edges_gtr_fa2", bundled_edges, width=2000, height=2000)

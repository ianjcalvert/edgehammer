from csv import DictReader
import networkx as nx
from tqdm import tqdm
import numpy as np
from dask.distributed import Client

from scipy.sparse import spdiags, coo_matrix
import scipy as sp
from core import bundle
from render_datashader import save_image

if __name__ == "__main__":
    node_numbers = {}
    nodes = {}
    edges = []

    edge_segments = [
            np.array([[0.1, i/1000, i], [0.9, i/1000, i]]) for i in range(1,1000)
        ]

    save_image("edges_basic", edge_segments, width=2000, height=2000)
    client = Client()
    for i in range(10):
        bundled_edges = bundle(edge_segments, 0.2, 0.9, iterations=i)
        save_image("bundled_edges_basic_%d" % i, bundled_edges, width=2000, height=2000)
